<?php
/**
 * Render 'Burger Menu' element to the front end
 *
 * @package Header_Builder
 * @subpackage Elements_Generator
 * @since 5.9.3
 */

/**
 * Main class used for rendering 'Burger Menu' element to the front end.
 *
 * @since 5.9.3 Introduced.
 */
class HB_Element_Burger_Menu extends HB_Element {
	/**
	 * Constructor.
	 *
	 * @since 5.9.3
	 *
	 * @param array $element {
	 *     The data to transform into HTML/CSS.
	 *
	 *     @type string $type
	 *     @type string $caption
	 *     @type string $id
	 *     @type string $category
	 *     @type array $options {
	 *           Array of element CSS properties and other settings.
	 *
	 *           @type string $width
	 *           @type string $spacing
	 *           @type string $alignment
	 *           @type string $cssDisplay
	 *           @type string $animation
	 *           @type string $thickness
	 *           @type string $boxRadius
	 *           @type string $barColor
	 *           @type string $boxColor
	 *           @type string $barColorHover
	 *           @type string $boxColorHover
	 *           @type string $boxBorder
	 *           @type string $margin
	 *           @type string $padding
	 *     }
	 * }
	 * @param int   $row_index     Numeric index for the row.
	 * @param int   $column_index  Numeric index for the column.
	 * @param int   $element_index Numeric index for the element.
	 */
	public function __construct( array $element, $row_index, $column_index, $element_index ) {
		parent::__construct( $element, $row_index, $column_index, $element_index );

		// Declare properties value.
		$this->width   = $this->get_option( 'barWidth', 20 );
		$this->spacing = $this->get_option( 'barSpacing', 5 );
		$this->align   = $this->get_option( 'alignment', 'left' );
		$this->inline  = $this->get_option( 'cssDisplay', 'block' );
		$this->animation  = $this->get_option( 'animation', 'none' );
		$this->thickness  = $this->get_option( 'barThickness', 5 );
		$this->box_radius = $this->get_option( 'boxCornerRadius', 3 );

		$this->bar_color  = $this->get_option( 'barColor',  array(
			'r' => 0,
			'g' => 0,
			'b' => 0,
			'a' => 1,
		) );
		$this->box_color  = $this->get_option( 'boxColor',  array(
			'r' => 0,
			'g' => 0,
			'b' => 0,
			'a' => 1,
		) );
		$this->bar_color_hover  = $this->get_option( 'barHoverColor',  array(
			'r' => 0,
			'g' => 0,
			'b' => 0,
			'a' => 1,
		) );
		$this->box_color_hover  = $this->get_option( 'boxHoverColor',  array(
			'r' => 0,
			'g' => 0,
			'b' => 0,
			'a' => 1,
		) );

		$this->box_border = $this->get_option( 'boxBorder', array(
			'active' => 'top',
			'top' => array(
				'width' => 1,
				'color' => array(
					'r' => 255,
					'g' => 255,
					'b' => 255,
					'a' => 1,
				),
			),
			'right' => array(
				'width' => 1,
				'color' => array(
					'r' => 255,
					'g' => 255,
					'b' => 255,
					'a' => 1,
				),
			),
			'bottom' => array(
				'width' => 1,
				'color' => array(
					'r' => 255,
					'g' => 255,
					'b' => 255,
					'a' => 1,
				),
			),
			'left' => array(
				'width' => 1,
				'color' => array(
					'r' => 255,
					'g' => 255,
					'b' => 255,
					'a' => 1,
				),
			),
		) );

		$this->padding = $this->get_option( 'padding', array(
			'top' => 0,
			'right' => 0,
			'bottom' => 0,
			'left' => 0,
		) );
		$this->margin  = $this->get_option( 'margin', array(
			'top' => 0,
			'right' => 0,
			'bottom' => 0,
			'left' => 0,
		) );

		// Enqueue the search style and HB script.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

	}

	/**
	 * Enqueue burger-menu style and HB script.
	 *
	 * @since 5.9.3
	 */
	public function enqueue_scripts() {
		wp_enqueue_style( 'hb-burger-menu', HB_ELEMENTS_ASSETS_URI . 'css/hb-burger-menu.css', array(), '5.9.3' );
		wp_enqueue_script( 'hb-burger-menu', HB_ELEMENTS_ASSETS_URI . 'js/hb-burger-menu.js', array( 'jquery' ), '5.9.3', true );
	}

	/**
	 * Generate the element's markup and style for use on the front-end.
	 *
	 * @since 5.9.3
	 *
	 * @return array {
	 *      HTML and CSS for the element, based on all its given properties and settings.
	 *
	 *      @type string $markup Element HTML code.
	 *      @type string $style Element CSS code.
	 * }
	 */
	public function get_src() {
		$hb_layout = new HB_CSS_Layout();
		$hb_css = new HB_CSS();

		// Burger Menu margin and padding.
		$padding = $hb_layout::trbl( $this->padding );
		$margin  = $hb_layout::trbl( $this->margin );

		// Burger Menu color.
		$bar_color = $hb_css::rgba( $this->bar_color );
		$box_color = $hb_css::rgba( $this->box_color );
		$bar_color_hover = $hb_css::rgba( $this->bar_color_hover );
		$box_color_hover = $hb_css::rgba( $this->box_color_hover );

		// Burger Menu inline block.
		$inline = $hb_layout::inline_block( $this->inline );

		// Burger Men border.
		$border = $hb_css::border( $this->box_border );

		$markup = sprintf( '
			<div id="%s" class="hb-burger-menu-el">
				<div class="hb-burger-menu-el__container fullscreen-style">
					<div class="hb-burger-menu-el__box">
						<div class="hb-burger-menu-el__bar"></div>
						<div class="hb-burger-menu-el__bar"></div>
						<div class="hb-burger-menu-el__bar"></div>
					</div>
				</div>
			</div>',
			esc_attr( $this->id )
		);

		$style = "
			#{$this->id} {
				text-align: {$this->align};
				margin: {$margin};
				{$inline}
			}
			#{$this->id} .hb-burger-menu-el__box {
				border-radius: {$this->box_radius}px;
				background-color: {$box_color};
				padding: {$padding};
				{$border};
			}
			#{$this->id} .hb-burger-menu-el__box:hover {
				background-color: {$box_color_hover};
			}
			#{$this->id} .hb-burger-menu-el__bar {
				width: {$this->width}px;
				margin-bottom: {$this->spacing}px;
				height: {$this->thickness}px;
				background-color: {$bar_color};
			}
			#{$this->id} .hb-burger-menu-el__bar:hover {
				background-color: {$bar_color_hover};
			}
		";

		return compact( 'markup', 'style' );
	}

}
