<?php
/**
 * Generator of CSS layout and position.
 *
 * @package Header_Builder
 * @since 5.9.3
 */

/**
 * Generator of CSS layout and position properties.
 *
 * @since 5.9.3
 */
class HB_CSS_Layout {

	/**
	 * Get inline block CSS styles.
	 *
	 * @since 5.9.3
	 *
	 * @param  boolean $display Element inline display. Default is false.
	 * @return string           Inline style CSS properties.
	 */
	public static function inline_block( $display = 'block' ) {
		if ( 'inline' !== $display || empty( $display ) ) {
			return '';
		}

		return '
			display: inline-block;
			vertical-align: top;
		';
	}

	/**
	 * Generate margin and padding properties.
	 *
	 * @since 5.9.3
	 *
	 * @todo  Should be removed later because we have a better format to set margin and padding.
	 *
	 * @param  array $margin_param  Margin parameter values.
	 * @param  array $padding_param Padding parameter values.
	 * @return array                 Margin and Padding in array.
	 */
	public static function margin_padding( $margin_param = array(), $padding_param = array() ) {
		// Set margin properties and values.
		$margin  = '';
		if ( ! empty( $margin_param ) ) {
			foreach ( $margin_param as $key => $value ) {
				$margin .= 'margin-' . $key . ': ' . $value . "px;\n";
			}
			unset( $key, $property, $value );
		}

		// Set padding properties and values.
		$padding = '';
		if ( ! empty( $padding_param ) ) {
			foreach ( $padding_param as $key => $value ) {
				$padding .= 'padding-' . $key . ': ' . $value . "px;\n";
			}
			unset( $key, $property, $value );
		}

		return compact( 'margin', 'padding' );
	}

	/**
	 * Set position properties value.
	 *
	 * @since 5.9.3
	 *
	 * @param  array $trbl Properties value.
	 * @return string      Combined property value.
	 */
	public static function trbl( $trbl = array() ) {
		return sprintf( '%spx %spx %spx %spx', $trbl['top'], $trbl['right'], $trbl['bottom'], $trbl['left'] );
	}

}
