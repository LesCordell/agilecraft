<?php
/**
 * Header Builder: Elements Generator
 *
 * For use in front end integration with Jupiter.
 *
 * @author Mehdi Shojaei <mehdi@artbees.net>
 *
 * @package Header_Builder
 * @subpackage Elements_Generator
 * @since 5.9.0
 */

/**
 * Takes JSON saved in database and output HTML and CSS on the front end.
 *
 * @SuppressWarnings(PHPMD)
 *
 * Warnings: get_src will be refactor later.
 *
 * @since 5.9.0
 *
 * We won't declare Mk_Header_Builder to be a singleton, as we might eventually find ourselves
 * needing different instances of it.
 */
class HB_Grid {
	/**
	 * Stores array data from JSON retrieved from options table.
	 *
	 * @since 5.9.0
	 * @access protected
	 * @var array $model The 'model' stored in 'artbees_header_builder' option.
	 */
	protected $model;

	/**
	 * Stores markup for all available data in $model.
	 *
	 * @since 5.9.0
	 * @access protected
	 * @var string $header_markup HTML output.
	 */
	protected $header_markup;

	/**
	 * Stores styles for all available data in $model.
	 *
	 * @since 5.9.0
	 * @access protected
	 * @var string $header_style CSS output.
	 */
	protected $header_style;

	/**
	 * Constructor.
	 *
	 * @since 5.9.0
	 */
	public function __construct() {
		$option = get_option( 'artbees_header_builder' );
		$option = json_decode( $option, true );
		$model = isset( $option['model'] ) ? $option['model'] : array();

		$this->model = $model;

		// Otherwise, throw an error if this file is missing.
		if ( ! class_exists( 'Mk_Header_Builder' ) ) {
			require_once( HB_INCLUDES_DIR . '/elements/class-hb-element.php' );
		}

		$output = $this->get_src( 'desktop' );

		$this->header_markup = $output['markup'];
		$this->header_style = $output['style'];
	}

	/**
	 * Output header builder markup on front-end.
	 *
	 * @since 5.9.0
	 */
	public function render_markup() {
		echo $this->markup(); // WPCS: XSS OK.
	}

	/**
	 * Output header builder styles on front-end.
	 *
	 * @since 5.9.0
	 */
	public function render_style() {
		echo $this->style(); // WPCS: XSS OK.
	}

	/**
	 * Return header builder styles on front-end.
	 *
	 * @since 5.9.0
	 */
	public function style() {
		return $this->header_style;
	}

	/**
	 * Return header builder markup on front-end.
	 *
	 * @since 5.9.0
	 */
	public function markup() {
		return $this->header_markup;
	}

	/**
	 * Output the header builder front end to Jupiter.
	 *
	 * @since 5.9.0
	 *
	 * @see get_device_src()
	 *
	 * @param string|array $devices List of devices to render. Accetps string values of
	 *     'desktop', 'tablet', 'mobile' or an array containing any combination of these values.
	 *     When 'all' is provided, it will be the equivalent of array('desktop', 'tablet', 'mobile').
	 * @return array {
	 *     @type string $markup Stringified HTML.
	 *     @type string $style Stringified CSS.
	 * }
	 */
	public function get_src( $devices = 'all' ) {
		$markup = '';
		$style = '';
		$accepted_device_list = array( 'desktop', 'tablet', 'mobile' );

		if ( 'all' === $devices ) {
			$devices = $accepted_device_list;
		}

		$devices = array_intersect( (array) $devices, $accepted_device_list );

		if ( empty( $devices ) ) {
			return compact( 'markup', 'style' );
		}

		foreach ( $devices as $device_name ) {
			$rendered = $this->get_device_src( $device_name );

			$markup .= sprintf( '<div class="%s">', esc_attr( "hb-device hb-$device_name" ) );
			$markup .= $rendered['markup'];
			$markup .= '</div>';

			$style .= $rendered['style'];
		}

		return compact( 'markup', 'style' );
	}

	/**
	 * Output the header builder front-end set for a specific device.
	 *
	 * @since 5.9.0
	 *
	 * @see get_row_src()
	 *
	 * @param string $device_name Device name. Accepts 'desktop', 'tablet', 'mobile'.
	 * @return array {
	 *     @type string $markup Stringified HTML.
	 *     @type string $style Stringified CSS.
	 * }
	 */
	public function get_device_src( $device_name ) {
		$device_model = array_safe_get( $this->model, $device_name, array() );

		$current_device_model = array_safe_get( $device_model, 'present', array() );

		$rows = array_safe_get( $current_device_model, 'rows', array() );
		$options = array_safe_get( $current_device_model, 'options', array() );
		$fullwidth = array_safe_get( $options, 'fullWidth', false );

		$markup = '';
		$style = '';

		foreach ( $rows as $row_index => $row_model ) {
			$rendered = $this->get_row_src( $row_model, $row_index, $fullwidth );

			$markup .= $rendered['markup'];
			$style .= $rendered['style'];
		}

		$markup .= '';

		return compact( 'markup', 'style' );
	}

	/**
	 * Output the header builder front-end set for a specific row.
	 *
	 * @since 5.9.0
	 *
	 * @see get_column_src()
	 *
	 * @param array  $row {
	 *     The data to transform into a single row of columns in the front-end.
	 *
	 *     @type string $type Type of element. Value of 'Row'.
	 *     @type string $caption Caption of element. Value of 'Row'.
	 *     @type string $category Category of element. Value of 'Row'.
	 *     @type string $id Unique ID for this element.
	 *     @type array $columns Array of columns, each containing an array of its own elements.
	 * }
	 * @param string $row_index Numeric index for the row.
	 * @param string $fullwidth Type of the row container width.
	 * @return array {
	 *     @type string $markup Stringified HTML.
	 *     @type string $style Stringified CSS.
	 * }
	 */
	public function get_row_src( array $row, $row_index, $fullwidth ) {
		$style = '';
		$markup = '';
		$columns = array_safe_get( $row, 'columns', array(
			array(
				'width' => 12,
			),
		) );

		if ( empty( $columns ) || ! is_numeric( $row_index ) ) {
			return compact( 'markup', 'style' );
		}

		// Gather the column content.
		$markup_content = '';
		$style_content = '';
		foreach ( $columns as $column_index => $column_model ) {
			$rendered = $this->get_column_src( $column_model, $row_index, $column_index );

			$markup_content .= $rendered['markup'];
			$style_content  .= $rendered['style'];
		}

		// Construct dynamic class name from element type.
		$for_class = sanitize_key( array_safe_get( $row, 'type' ) );
		$for_file  = sanitize_key( array_safe_get( $row, 'type' ) );

		$class_name = 'HB_Element_' . ucwords( $for_class );
		$class_file = HB_INCLUDES_DIR . '/elements/class-hb-element-' . strtolower( $for_file ) . '.php';

		// Render row markup and style.
		$rendered = array();
		if ( ! class_exists( $class_name ) ) {
			if ( ! file_exists( $class_file ) ) {
				return compact( 'markup','style' );
			}

			include_once( $class_file );
		}

		$instance = new $class_name( $row, $row_index, $markup_content );
		$rendered = $instance->get_src();

		// Set the markup and style values.
		$markup = sprintf(
			'<div id="%s" class="hb-row %s hb-equal-height-columns">
				<div class="%s">
				%s
				</div>
				<div class="clearfix"></div>
			</div>',
			array_safe_get( $row, 'id', '' ),
			esc_attr( 'hb-row-' . $row_index ),
			esc_attr( 'hb-container' . ( $fullwidth ? '-fluid' : '' ) ),
			$markup_content
		);
		$style  = $rendered['style'];

		// Then merge with current content and style of columns.
		$style .= $style_content;

		return compact( 'markup', 'style' );
	}

	/**
	 * Output the header builder front-end set for a specific column.
	 *
	 * @since 5.9.0
	 *
	 * @see get_element_src()
	 *
	 * @param array  $column {
	 *     The data to transform into a single column in the front-end.
	 *
	 *     @type string $type Type of element. Value of 'Row'.
	 * }
	 * @param string $row_index    Numeric index for the row.
	 * @param string $column_index Numeric index for the column.
	 * @return array {
	 *     @type string $markup Stringified HTML.
	 *     @type string $style Stringified CSS.
	 * }
	 */
	public function get_column_src( array $column, $row_index, $column_index ) {
		$elements = array_safe_get( $column, 'elements', array() );
		$markup = '';
		$style = '';

		if ( ! is_numeric( $row_index ) || ! is_numeric( $column_index ) ) {
			return  compact( 'markup', 'style' );
		}

		$markup = sprintf( '<div class="hb-col-md-%s" id="%s">', $column['width'], $column['id'] );

		// Variable declaration for inline block.
		$inline_container = false;
		$inline_element = array(
			'left' => '',
			'center' => '',
			'right' => '',
		);
		$count_elements = count( $elements );

		foreach ( $elements as $element_index => $element_model ) {
			--$count_elements;
			$rendered = $this->get_element_src( $element_model, $row_index, $column_index, $element_index );

			$style .= $rendered['style'];

			// MANDATORY: Check display is exist or not.
			$element_display = ! empty( $element_model['options']['cssDisplay'] ) ? $element_model['options']['cssDisplay'] : 'block';

			// MANDATORY: If display is inline.
			if ( 'inline' === $element_display ) {
				$element_align = ! empty( $element_model['options']['alignment'] ) ? $element_model['options']['alignment'] : 'left';

				// MANDATORY: Display is inline, if no inline container exist, create open tag container.
				if ( false === $inline_container ) {
					$markup .= '<div class="hb-inline-container">';
					$inline_container = true;
				}

				// Save element with inline to $inline_element base on alignment.
				$inline_element[ $element_align ] .= $rendered['markup'];

				// Clear recent markup.
				$rendered['markup'] = '';

				if ( $count_elements > 0 ) {
					continue;
				}
			}

			// MANDATORY: If inline container exist, place all the elements, and close container.
			if ( true === $inline_container ) {
				// Inline container is exist, place all the elements, and close container.
				// OPTIONAL: We will generate the container even it's empty to improve performance.
				$markup .= sprintf( '
					<div class="hb-inline-container__left">%s</div>
					<div class="hb-inline-container__center">%s</div>
					<div class="hb-inline-container__right">%s</div>',
					$inline_element['left'],
					$inline_element['center'],
					$inline_element['right']
				);

				// Close container and set inline_container is false (closed).
				$markup .= '</div>';
				$inline_container = false;
				$inline_element = array(
					'left' => '',
					'center' => '',
					'right' => '',
				);
			}

			$markup .= $rendered['markup'];
		}// End foreach().

		$markup .= '</div>';

		$markup_content = '';
		$style_content = '';
		// Construct dynamic class name from element type.
		$for_class = sanitize_key( array_safe_get( $column, 'category' ) );
		$for_file  = sanitize_key( array_safe_get( $column, 'category' ) );

		$class_name = 'HB_Element_' . ucwords( $for_class );
		$class_file = HB_INCLUDES_DIR . '/elements/class-hb-element-' . strtolower( $for_file ) . '.php';

		// Render row markup and style.
		$rendered = array();
		if ( ! class_exists( $class_name ) ) {
			if ( ! file_exists( $class_file ) ) {
				return compact( 'markup','style' );
			}

			include_once( $class_file );
		}

		$instance = new $class_name( $column, $column_index, $markup_content );
		$rendered = $instance->get_src();
		$style_content  .= $rendered['style'];

		// Then merge with current content and style of columns.
		$style .= $style_content;

		return compact( 'markup', 'style' );
	}

	/**
	 * Output the header builder front-end set for a specific element type.
	 *
	 * @since 5.9.0
	 *
	 * @param array  $element {
	 *     The data to transform into a single column in the front-end.
	 *
	 *     @type string $type Type of element. Value of 'Row'.
	 *
	 * }
	 * @param string $row_index     Nth row to render (0 indexed).
	 * @param string $column_index  Nth column to render (0 indexed).
	 * @param string $element_index Nth column to render (0 indexed).
	 * @return array {
	 *     @type string $markup Stringified HTML.
	 *     @type string $style Stringified CSS.
	 * }
	 */
	public function get_element_src( array $element, $row_index, $column_index, $element_index ) {
		$style = '';
		$markup = '';

		if ( ! isset( $element['type'] ) ||
			! is_string( $element['type'] ) ||
			! is_numeric( $row_index ) ||
			! is_numeric( $column_index ) ||
			! is_numeric( $element_index ) ) {
			return compact( 'markup', 'style' );
		}

		// Construct dynamic class name from element type.
		$for_class = str_replace( '-', '_', sanitize_key( $element['type'] ) );
		$for_file = str_replace( '_', '-', sanitize_key( $element['type'] ) );

		$class_name = 'HB_Element_' . ucwords( $for_class, '_' );
		$class_file = HB_INCLUDES_DIR . '/elements/class-hb-element-' . strtolower( $for_file ) . '.php';

		if ( ! class_exists( $class_name ) ) {
			if ( ! file_exists( $class_file ) ) {
				return compact( 'markup','style' );
			}

			include_once( $class_file );
		}

		$element_instance = new $class_name( $element, $row_index, $column_index, $element_index );

		return $element_instance->get_src();
	}
}

new HB_Grid();
