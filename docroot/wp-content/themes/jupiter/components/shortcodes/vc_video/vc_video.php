<?php
global $wp_embed;

include trailingslashit( dirname( __FILE__ ) ) . 'config.php';

$id = Mk_Static_Files::shortcode_id();

if ( empty( $host ) || ! in_array( $host, array( 'self_hosted', 'social_hosted' ) ) ) {
	_e( 'Invalid video type.', 'mk_framework' );
	return;
}

// Check self hosted video data
if ( $host == 'self_hosted' && empty( $mp4 ) && empty( $webm ) ) {
	_e( 'Self hosted video file is empty.', 'mk_framework' );
	return;
}

// Check social hosted video data
if ( $host == 'social_hosted' && empty( $link ) ) {
	_e( 'Social hosted video URL is empty.', 'mk_framework' );
	return;
}

// Set video wrapper attributes
$wpb_wrapper_attr = '';
if ( intval( $max_width ) ) {
	$wpb_wrapper_attr .= ' style="max-width:' . $max_width . 'px"';
}

// Set video container data attributes
$video_container_attr = '';
$video_container_attr .= ' data-id="' . $id . '"';
$video_container_attr .= ' data-source="' . $host . '"';
$video_container_attr .= ' data-autoplay="' . ( ( 'true' == $autoplay ) ? '1' : '0' ) . '"';
$video_container_attr .= ' data-target="' . $play_target . '"';

$thumbnail_image_src = ( $autoplay == 'false' && $custom_thumbnail == 'true' && ! empty( $thumbnail_image ) ) ? wp_get_attachment_url( $thumbnail_image ) : '';

$play_icon = !empty( $play_icon ) ? (strpos($play_icon, 'mk-') !== FALSE) ? $play_icon : ( 'mk-'.$play_icon.'' ) : '';
?>
<div class="wpb_video_widget <?php echo get_viewport_animation_class( $animation ) . $el_class . ' ' . $visibility; ?>">
	<div class="wpb_wrapper"<?php echo $wpb_wrapper_attr; ?>>
		<?php mk_get_view( 'global', 'shortcode-heading', false, [ 'title' => $title ] ); ?>
		<div id="video-container-<?php echo $id; ?>" class="video-container"<?php echo $video_container_attr; ?> <?php echo get_schema_markup( 'video' ); ?>>
			<div id="video-player-<?php echo $id; ?>" class="video-player">
			<?php if ( $host == 'self_hosted' ): ?>
			<video poster="<?php echo $poster_image; ?>"" preload="auto" controls="controls">

				<?php if ( !empty( $mp4 ) ) { ?>

					<source type="video/mp4" src="<?php echo $mp4; ?>" />

				<?php } if ( !empty( $webm ) ) { ?>

					<source type="video/webm" src="<?php echo $webm; ?>" />

				<?php } ?>
			</video>
			<?php endif; ?>
			<?php if ( $host == 'social_hosted' ): ?>
				<?php echo $wp_embed->run_shortcode( '[embed width="1140" height="641"]' . $link . '[/embed]' ); ?>
			<?php endif; ?>
			</div>
			<?php if( $thumbnail_image_src ): ?>
			<div id="video-thumbnail-<?php echo $id; ?>" class="video-thumbnail <?php echo $play_icon_animation; ?>" style="background-image: url('<?php echo $thumbnail_image_src; ?>');">
				<div class="video-thumbnail-overlay">
					<?php Mk_SVG_Icons::get_svg_icon_by_class_name(true, $play_icon, $play_icon_size, $play_icon_color); ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>