<?php

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_Types_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it Types for your posts

function create_Types_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI


  $labels = array(
    'name' => _x( 'Topics', 'taxonomy general name' ),
    'singular_name' => _x( 'Topic', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Topics' ),
    'all_items' => __( 'All Topics' ),
    'parent_item' => __( 'Parent Topic' ),
    'parent_item_colon' => __( 'Parent Topic:' ),
    'edit_item' => __( 'Edit Topic' ),
    'update_item' => __( 'Update Topic' ),
    'add_new_item' => __( 'Add New Topic' ),
    'new_item_name' => __( 'New Topic Name' ),
    'menu_name' => __( 'Resource Topics' ),
  );

  register_taxonomy('Topics',array('library-resource'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'resource-topic' ),
  ));


  $labels = array(
    'name' => _x( 'Types', 'taxonomy general name' ),
    'singular_name' => _x( 'Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Types' ),
    'all_items' => __( 'All Types' ),
    'parent_item' => __( 'Parent Type' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item' => __( 'Edit Type' ),
    'update_item' => __( 'Update Type' ),
    'add_new_item' => __( 'Add New Type' ),
    'new_item_name' => __( 'New Type Name' ),
    'menu_name' => __( 'Resource Types' ),
  );

// Now register the taxonomy

  register_taxonomy('Types',array('library-resource'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'resource-type' ),
  ));

}

add_action( 'init', 'create_customposttype' );
function create_customposttype() {
	$labels = array(
		'name'               => _x( 'Events', 'post type general name', 'cpt-event' ),
		'singular_name'      => _x( 'Event', 'post type singular name', 'cpt-event' ),
		'menu_name'          => _x( 'Events', 'admin menu', 'cpt-event' ),
		'name_admin_bar'     => _x( 'Event', 'add new on admin bar', 'cpt-event' ),
		'add_new'            => _x( 'Add New', 'event', 'cpt-event' ),
		'add_new_item'       => __( 'Add New Event', 'cpt-event' ),
		'new_item'           => __( 'New Event', 'cpt-event' ),
		'edit_item'          => __( 'Edit Event', 'cpt-event' ),
		'view_item'          => __( 'View Event', 'cpt-event' ),
		'all_items'          => __( 'All Events', 'cpt-event' ),
		'search_items'       => __( 'Search Events', 'cpt-event' ),
		'parent_item_colon'  => __( 'Parent Events:', 'cpt-event' ),
		'not_found'          => __( 'No events found.', 'cpt-event' ),
		'not_found_in_trash' => __( 'No events found in Trash.', 'cpt-event' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'cpt-event' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'event' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'event', $args );

	$labels_lr = array(
		'name'               => _x( 'Library Resources', 'post type general name', 'cpt-library' ),
		'singular_name'      => _x( 'Library Resource', 'post type singular name', 'cpt-library' ),
		'menu_name'          => _x( 'Library Resources', 'admin menu', 'cpt-library' ),
		'name_admin_bar'     => _x( 'Library Resource', 'add new on admin bar', 'cpt-library' ),
		'add_new'            => _x( 'Add New', 'library-resource', 'cpt-library' ),
		'add_new_item'       => __( 'Add New Library Resource', 'cpt-library' ),
		'new_item'           => __( 'New Library Resource', 'cpt-library' ),
		'edit_item'          => __( 'Edit Library Resource', 'cpt-library' ),
		'view_item'          => __( 'View Library Resource', 'cpt-library' ),
		'all_items'          => __( 'All Library Resources', 'cpt-library' ),
		'search_items'       => __( 'Search Library Resources', 'cpt-library' ),
		'parent_item_colon'  => __( 'Parent Library Resources:', 'cpt-library' ),
		'not_found'          => __( 'No Library Resources found.', 'cpt-library' ),
		'not_found_in_trash' => __( 'No Library Resources found in Trash.', 'cpt-library' )
	);

	$args_lr = array(
		'labels'             => $labels_lr,
        'description'        => __( 'Description.', 'cpt-library' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'library-resource' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),

	);

	register_post_type( 'library-resource', $args_lr );
}

function add_theme_scripts() {
	wp_enqueue_style( 'events',  '/wp-content/themes/jupiter-child/events.css',null,null,'all');

	wp_enqueue_style( 'library',  '/wp-content/themes/jupiter-child/library.css',null,null,'all');
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


?>
