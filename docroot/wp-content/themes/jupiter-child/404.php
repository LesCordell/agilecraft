<?php
/*
** 404.php
** mk_build_main_wrapper : builds the main divisions that contains the content. Located in framework/helpers/global.php
** mk_get_view gets the parts of the pages, modules and components. Function located in framework/helpers/global.php
*/

get_header();

$output .= '<div id="theme-page" class="master-holder clearfix">';

$output .= '<div class="bg_404" style="text-align:center;">';

$output .= '<a href="/"><img src="http://agilecraft.w3bsolution.com/wp-content/uploads/2017/09/yeti-404.png" /></a>';

$output .= "<h3>Looks like this page doesn't exist</h3><br />";

$output .= '<div id="mk-button-3" class="mk-button-container text-center">';

$output .= '<a href="/" class="mk-button js-smooth-scroll mk-button--dimension-flat mk-button--size-medium mk-button--corner-rounded text-color-light _ relative text-center font-weight-700 no-backface  letter-spacing-1 inline">Take me back</a></div><br />';

$output .= '<p class="">or <a class="mk-fullscreen-trigger" href="#">search</a> for a page</p><br />';

$output .= '</div></div>';

echo $output;


get_footer();
