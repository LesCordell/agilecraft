(function($) {
    $(function() {
        var _NORESULTSMESSAGE = 'No results for this search.';
        // Unbind search overlay events so it doesn't hide when we interact
        // with ajax results. As this script loads after it's bound,
        // unbind the click event on click.
        var $searchOverlay = $('.mk-fullscreen-search-overlay');
        var $fullScreenSearchForm = $('#mk-fullscreen-searchform');
        var $responsiveSearchForm = $('.responsive-searchform');
        if ($searchOverlay.length) {
            $searchOverlay.on('click', function(event) {
                var $target = $(event.target);
                if(!$target.is('a') || $target.is('.page-number')) {
                    event.stopPropagation(); event.preventDefault();
                    event.stopImmediatePropagation();
                    if($target.is('.fullscreen-search-icon, .mk-svg-icon') && $fullScreenSearchForm.length) {
                        $fullScreenSearchForm.submit();
                    }
                    return false;
                }
            });
        }
        /**
         * loads the search results into the container by scrubbing an html
         * page for the correct elements.
         *
         * @param $html - jQuery html response object containing search items.
         */
        function loadSearchResults($html) {
            var $resultsContainer = $('.mk-search-results-container');
            if ($resultsContainer.length) {
                var $resultItems = $html.find('.mk-search-loop .search-result-item');
                var $pagination = $html.find('.mk-search-loop .mk-pagination');
                $resultsContainer.empty();
                if ($resultItems.length) {
                    $resultsContainer.removeClass('empty');
                    $resultItems.each(function (i, el) {
                        var $el = $(el);
                        $resultsContainer.append($el);
                    });
                    if ($pagination.length) {
                        $resultsContainer.append($pagination);
                        // Bind any pagination events recursively.
                        var $pageItems = $resultsContainer.find('.mk-pagination a');
                        if ($pageItems.length) {
                            $pageItems.on('click', function (event) {
                                $target = $(event.target);
                                if($target.is('a') && $target.parents('.mk-pagination').length) {
                                    event.stopPropagation(); event.preventDefault();
                                    event.stopImmediatePropagation();
                                    $.get($target.attr('href'), function(res) {
                                        loadSearchResults($(res));
                                    });
                                    return false;
                                }
                            });
                        }
                    }
                    $resultsContainer.animate({ scrollTop: (0) }, 'slow');
                }
                else {
                    var $noResults = $('<div class="mk-no-results">' + _NORESULTSMESSAGE + '</div>');
                    $resultsContainer.addClass('empty');
                    $resultsContainer.append($noResults);
                }
            }
        }


        if ($responsiveSearchForm.length || $fullScreenSearchForm.length) {
            $('#mk-fullscreen-search-input').attr('autocomplete', 'off');
            $('.responsive-searchform > input').attr('autocomplete', 'off');
            /**
             * On search submit, ajax submit the form, parse the html
             * response for search item elements and append them to the
             * results container.
             */
            $responsiveSearchForm.on('submit', function(event) {
                event.preventDefault();
                $.get('/?' + $responsiveSearchForm.serialize(), function(res) {
                    loadSearchResults($(res));
                });
            });
            $fullScreenSearchForm.on('submit', function(event) {
                event.preventDefault();
                $.get('/?' + $fullScreenSearchForm.serialize(), function(res) {
                    loadSearchResults($(res));
                });
            });
        }
    });
})(jQuery);
