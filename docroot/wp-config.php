<?php
/** Enable W3 Total Cache */
define('WP_CACHE', false); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'agilecraft');

/** MySQL database username */
define('DB_USER', 'agilecraft');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Jbb_H|gw|Ex[`t-sXT:*Z66rA``p~#DVq_1VH|hV J>aT*Q7nc0Yqr4?zx{Ktdz7');
define('SECURE_AUTH_KEY',  '/{5b{gyaju3nPmZf)q:r&x&mA2470yyU1]m$ 2LD$/ln+!$4t$.m}VoK7~IAKd])');
define('LOGGED_IN_KEY',    'zehayhgc[>ng~Y9l4!C>O?e,Rpgn-V3vKa(^CZNM%S.#ECmchdwy&-Rem(cV`WeG');
define('NONCE_KEY',        '7r~hUb_6,UP?kr{n)SW ` r~-#0*&G7eSSf<4EIzKVGsZT!f-7m7zs$#BWWkmY!U');
define('AUTH_SALT',        'Ggir:#>a{;fYAVPu(U!?!eh N]_?}m,(tWivZnl#yjw*!x)?#t+ags1a?>t!nH+:');
define('SECURE_AUTH_SALT', 'V,V+J<F*3x[L0h^3ATo(W^600Xg9J+!+: N(LpMji%Do2&J.wukGLRE!EC,x/J54');
define('LOGGED_IN_SALT',   'O$w#E*1c=$+RuskTQeTn[;5wyR:[Pw:Bl@GEYqGa+x|.$w4A{%dnh.VQN +G=>#,');
define('NONCE_SALT',       '*FnVt#qfWa%x]agd* x<do7XxK[xYF*y43&|cN>,rct9EyT*yXZT`]C Wqc9G8qH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/** Memory Limit */
define('WP_MEMORY_LIMIT', '256M');
define( 'WP_MAX_MEMORY_LIMIT', '256M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
